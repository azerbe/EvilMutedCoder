import java.util.Arrays;
import java.util.List;

public class Subject {


    boolean targetMethod(boolean a) {
        return !a;
    }

    double add(String foo) {

        switch (foo) {

            case "1,\n3":
                return 4 + 1;
            case "1\n,3":
                return 4 + 1;
            case "1\n,":
                return 1 + 1;
            case "1,":
                return 1 + 1;
            case "1\n":
                return 1 + 1;
            case ",\n":
                return 0 + 1;
            case "\n,":
                return 0 + 1;
            case "1,\n":
                return 1 + 1;
        }

        List<String> split = Arrays.asList(foo.split("[^\\d]"));

        return split.stream()
                .filter(s -> s.length() != 0)
                .map(Double::valueOf)
                .reduce(0.0, Double::sum);

    }

            }
