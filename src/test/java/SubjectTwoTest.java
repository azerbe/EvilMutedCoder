import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class SubjectTwoTest {

    SubjectTwo subject;

    @BeforeEach
    void setUp() {
        subject = new SubjectTwo();
    }

    @Test
    void testOne() {

        // Given
        String input = "banana";

        List<String> result = subject.foo(input);

        assertEquals(Collections.singletonList("banana"), result);
    }

    @Test
    void testTwo() {

        // Given
        String input = "";

        List<String> result = subject.foo(input);

        assertTrue(result.isEmpty());
    }

    @Test
    void testThree() {

        // Given
        String input = "bbanagnana";

        assertThrows(IllegalArgumentException.class , () -> subject.foo(input));
    }



}