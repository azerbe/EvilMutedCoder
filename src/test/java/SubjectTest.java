import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import java.util.concurrent.ThreadLocalRandom;
import static org.junit.jupiter.api.Assertions.*;
class SubjectTest {
    Subject subject;
    @BeforeEach
    void setup() {
        subject = new Subject();
    }
    @Test
    void test() {

        int randInt = ThreadLocalRandom.current().nextInt(1,10);

        assertEquals(subject.add(Integer.toString(randInt)), randInt);
        assertEquals(subject.add(""), 0);
        assertEquals(subject.add("1,1"),2);

        String s = "";
        int sum = 0;
        int j = 0;

        for(int i = 0; i<randInt; i++){
            j = ThreadLocalRandom.current().nextInt(1,10);
            s = s + Integer.toString(j) + "\n";
            sum += j;
        }
        s = s + Integer.toString(j);
        sum += j;

        assertEquals(subject.add(s),sum);


        assertNotEquals(subject.add("1,\n3"), 4);
        assertNotEquals(subject.add("1\n,3"), 4);
        assertNotEquals(subject.add("1\n,"), 1);
        assertNotEquals(subject.add("1,"), 1);
        assertNotEquals(subject.add("1\n"), 1);
        assertNotEquals(subject.add(",\n"), 0);
        assertNotEquals(subject.add("\n,"), 0);
        assertNotEquals(subject.add("1,\n"), 1);

        assertEquals(subject.add("//A\n1"), 1);
        assertEquals(subject.add("//;\n3;4"), 7);
        assertEquals(subject.add("//§\n3§4"), 7);

    }
}